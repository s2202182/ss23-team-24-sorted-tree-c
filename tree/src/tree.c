#include "tree.h"

/**
 * Please correct the contents of this file to make sure all functions here do what they are supposed to do if you find
 * that they do not work as expected.
 */

// Tree function: you are allowed to change the contents, but not the method signature
Tree* tree_create(){
    Tree *tree = malloc(sizeof(Tree));

    if (tree != NULL){
        tree->root = NULL;
    }

    return tree;
}

// Helper function: you are allowed to change this to your preferences
void tree_node_delete(Node* node) {

    if (node == NULL) return;

    tree_node_delete(node->left);
    tree_node_delete(node->right);
    
    //free(node->name);

    free(node);
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_delete(Tree* tree) {
    tree_node_delete(tree->root);
    
    free(tree);
}

//node_depth and max_depth used for sorting and balancing

int node_depth(Node *node){
    if(node == NULL){
        return 0;
    }
    else{
        int leftDepth = node_depth(node->left);
        int rightDepth = node_depth(node->right);
        if(leftDepth > rightDepth){
            return leftDepth + 1;
        }
        else{
            return rightDepth + 1;
        }
    }
}

int max_depth(int leftDepth, int rightDepth){
    if(leftDepth > rightDepth){
        return leftDepth;
    }
    else{
        return rightDepth;
    }
}

int depth_difference(Node *node){
    int difference = 0;
    if(node == NULL){
        return 0;
    }
    else{
        if(node->left != NULL){
            difference = difference + node_depth(node->left);
        }
        if(node->right != NULL){
            difference = difference - node_depth(node->right);
        }
    }
    return difference;
}



Node *rotate_left(Node *node){
    Node *temp = node;
    Node *temp2 = temp->left;
    temp->left = temp2->right;
    temp2->right = temp;
    return temp2;
}

Node *rotate_right(Node *node){
    Node *temp = node;
    Node *temp2 = temp->right;
    temp->right = temp2->left;
    temp2->left = temp;
    return temp2;
}

Node *balance_tree(Node *node){
    
    if(node != NULL){
        if(node->left != NULL){
            node->left = balance_tree(node->left);
        }
        if(node->right != NULL){
            node->right = balance_tree(node->right);
        }
    }
    
    //balancing tree based on depths

    int balance = depth_difference(node);
    if(balance > 1){
        if(depth_difference(node->left) <= -1){
            node = rotate_left(node);
            return rotate_right(node);
        }
        else{
            return rotate_left(node);
        }
    }
    else if(balance < -1){
        if(depth_difference(node->right) >= 1){
            node = rotate_right(node);
            return rotate_left(node);
        }
        else{
            return rotate_right(node);
        }
    }else {
        return node;
    }
}

// Helper function: you are allowed to change this to your preferences
void node_insert(Node* node, int age, char* name) {
    if (node == NULL) return;

    //decide where to insert based on age
    if (age <= node->age) {
        if (node->left == NULL) {
            Node* temp = calloc(1, sizeof(Node));
            char* tempName = malloc(sizeof(char) * strlen(name) + 1);
            strcpy(tempName, name);
            temp->name = tempName;
            temp->age = age;
            node->left = temp;
        }
        else {
            node_insert(node->left, age, name);
        }
    }
    else {
        if (node->right == NULL) {
            Node* temp = calloc(1, sizeof(Node));
            char* tempName = malloc(sizeof(char) * strlen(name) + 1);
            strcpy(tempName, name);
            temp->name = tempName;
            temp->age = age;
            node->right = temp;
        }
        else {
            node_insert(node->right, age, name);
        }
    }

    
    int leftDepth = (node->left == NULL ? 0 : node->left->height);
    int rightDepth = (node->right == NULL ? 0 : node->right->height);
    node->height = max_depth(leftDepth, rightDepth) + 1;
}

Node *delete_node(Tree *tree, Node *rootNode, Node* node){
    if(rootNode == NULL || node == NULL){
        return NULL;
    }
    else if(node->age < rootNode->age){
        if(rootNode->left!=NULL){
            rootNode->left = delete_node(tree, rootNode->left, node);
        }
        else{
            return NULL;
        }
    }
    else if(node->age > rootNode->age){
        if(rootNode->right != NULL){
            rootNode->right = delete_node(tree, rootNode->right, node);
        } else {
            return NULL;
        }
    }
    else{
        if(rootNode->left == NULL || rootNode->right == NULL){
            Node *temp = NULL;
            if(rootNode->left){
                Node *temp = rootNode->left;
            }else {
                Node *temp = rootNode->right;
            }
            if(temp == NULL){
                temp = rootNode;
                rootNode = NULL;
            }else{
                *rootNode = *temp;
            }
            
        }
        else{
            Node *temp = rootNode->right;
            while(temp->left != NULL){
                temp = temp->left;
            }
            rootNode->age = temp->age;
            strcpy(rootNode->name, temp->name);
            rootNode->right = delete_node(tree, rootNode->right, temp);
        }
    }
    return rootNode;
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_insert(Tree* tree, int age, char* name) {
    if (tree->root == NULL) {
        Node *node = calloc(1,sizeof(Node));

        //check allocation
        if (node != NULL){
            node->age = age;
            node->name = malloc(sizeof(char)*strlen(name) + 1);
            strcpy(node->name, name);
            tree->root = node;
        }

    } else {
        node_insert(tree->root, age, name);
    }

    if(tree->root != NULL){
        tree->root = balance_tree(tree->root);
    }
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_erase(Tree* tree, int age, char* name) {
    Node* node = tree_find(tree, age, name);
    printf("\n Erasing data: %d, %s\n", age, name);
    if(node != NULL){
        tree->root = delete_node(tree, tree->root, node);
        if(tree->root != NULL){
            tree->root = balance_tree(tree->root);
        }
    }else {
        printf("\nNode not found\n");
    }
}

// Helper function: you are allowed to change this to your preferences
void tree_print_node(Node* node){
    if (node == NULL) {
        printf("null");
        return;
    }

    printf("[");
    printf("{\"%d\":\"%s\"},", node->age, node->name);
    tree_print_node(node->left);
    printf(",");
    tree_print_node(node->right);
    printf("]");
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_print(Tree* tree, int printNewline){
    if (tree == NULL) {
        printf("null");
        return;
    }
    tree_print_node(tree->root);

    if (printNewline){
        printf("\n");
    }
}

// Helper function: you are allowed to change this to your preferences
Node* node_find(Node* node, int age, char* name) {
    if (node == NULL){
        return NULL;
    }

    if (node->age == age && (strcmp(node->name, name) == 0)){
        return node;
    }

    if (age <= node->age) {
        return node_find(node->left, age, name);

    } else{
        return node_find(node->right, age, name);
    }
}

// Tree function: you are allowed to change the contents, but not the method signature
Node* tree_find(Tree* tree, int age, char* name) {

    return node_find(tree->root, age, name);

}
#!/bin/bash

mkdir build
export CC="afl-clang"
# export AFL_USE_ASAN=1
# export CFLAGS="-fsanitize=address -m32"
cmake -S . -B build
cd build
make

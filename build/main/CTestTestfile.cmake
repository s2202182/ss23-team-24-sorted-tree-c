# CMake generated Testfile for 
# Source directory: /src/main
# Build directory: /src/build/main
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("tests")

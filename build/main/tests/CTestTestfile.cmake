# CMake generated Testfile for 
# Source directory: /src/main/tests
# Build directory: /src/build/main/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_main "/usr/bin/bash" "/src/main/tests/test_main.sh" "/src/build/main/src/main")
set_tests_properties(test_main PROPERTIES  WORKING_DIRECTORY "/src/main/tests" _BACKTRACE_TRIPLES "/src/main/tests/CMakeLists.txt;2;add_test;/src/main/tests/CMakeLists.txt;0;")
add_test(test_sorted "/usr/bin/bash" "/src/main/tests/test_sorted.sh" "/src/build/main/src/main")
set_tests_properties(test_sorted PROPERTIES  WORKING_DIRECTORY "/src/main/tests" _BACKTRACE_TRIPLES "/src/main/tests/CMakeLists.txt;3;add_test;/src/main/tests/CMakeLists.txt;0;")

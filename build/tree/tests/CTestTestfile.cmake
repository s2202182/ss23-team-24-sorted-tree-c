# CMake generated Testfile for 
# Source directory: /src/tree/tests
# Build directory: /src/build/tree/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_tree "/src/build/tree/tests/test_tree")
set_tests_properties(test_tree PROPERTIES  _BACKTRACE_TRIPLES "/src/tree/tests/CMakeLists.txt;4;add_test;/src/tree/tests/CMakeLists.txt;0;")

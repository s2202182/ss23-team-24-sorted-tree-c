# CMake generated Testfile for 
# Source directory: /src/tree
# Build directory: /src/build/tree
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("tests")

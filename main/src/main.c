#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "tree.h"
#include <ctype.h>


//input buffer
char* dynamic_buffer(){
    char* buffer = NULL;
    char* temp = NULL;
    int size = 0;
    int read = 0;
    int character = EOF;

    while(character) {
        character = getc(stdin);
        if (character == EOF || character == '\n') {
            character = 0;
        }
        if (!isascii(character)){
        continue;
        }

        if(size <= read){
            size += (read+1)*sizeof(char);
            temp = realloc(buffer, size);
            if(temp == NULL){
                free(buffer);
                break;
            }
            buffer = temp;
        }
        buffer[read++] = character;
    }

    return buffer;
}

// You are allowed to change anything about this function to fix it
int main() {
    char* commandBuffer = NULL;

    Tree *tree = tree_create();

    for(;;) {
        commandBuffer = dynamic_buffer();

        // Quit on EOF or 'q'
        if (feof(stdin) ||  strcmp(commandBuffer, "q") == 0){
            break;
        }
        tree = handleString(commandBuffer, tree);
        free(commandBuffer);
    };

    free(tree);

    return 0;
}

/**
 * Handle a command entered into the program
 *
 * You are allowed to change anything about this function to fix it
 * @param command The command string to handle
 */
Tree *handleString(char command[], Tree *tree){
    if (command == NULL){
        fprintf(stderr, "Invalid command; null pointer\n");
        return tree;
    }

    switch(command[0]){
        case 'i':
            insert(command, tree);
            break;
        case 'e':
            erase(command, tree);
            break;
        case 'c':
            check(command, tree);
            break;
        case 'p':
            tree_print(tree, 1);
            break;
        case 'x':
            tree_delete(tree);
            return tree_create();
        default:
            fprintf(stderr, "Invalid command string: %s\n", command);
            break;
    }

    return tree;
}

// You are allowed to change anything about this function to fix it
Tree *insert(char* command, Tree *tree) {
    int age;

    if (2 != sscanf(command, "i %d %s", &age, command)) {
        fprintf(stderr, "Failed to parse insert command: not enough parameters filled\n");
        return NULL;
    }

    if(tree == NULL) {
        tree = tree_create();
    }

    if(age>100){
        fprintf(stderr, "Failed to parse insert command: age is over the limits\n");
        return NULL;
    }else if(age<0){
        fprintf(stderr, "Failed to parse insert command: age is over the limits\n");
        return NULL;
    }

    char* name = malloc(sizeof(char) * strlen(command) + 1);
    strcpy(name, command);

    Node *copy = tree_find(tree, age, name);
    if(copy != NULL){
        fprintf(stderr, "Failed to insert: person already exists\n");
    } else {
        tree_insert(tree, age, name);
    }
    free(name);
    return tree;
}

// You are allowed to change anything about this function to fix it
void erase(char* command, Tree *tree) {
    int age;

    if (2 != sscanf(command, "e %d %s", &age, command)) {
        fprintf(stderr, "Failed to parse erase command: not enough parameters filled\n");
    }

    if(age>100){
        fprintf(stderr, "Failed to parse erase command: age is over the limits\n");
        return;
    }else if(age<0){
        fprintf(stderr, "Failed to parse insert command: age is over the limits\n");
        return ;
    }

    char* name = malloc(sizeof(char) * (strlen(command) + 1));
    strcpy(name, command);
    
    tree_erase(tree, age, name);
    free(name);
}

// You are allowed to change anything about this function to fix it
void check(char* command, Tree* tree) {
    int age;

    if (2 != sscanf(command, "c %d %s", &age, command)) {
        fprintf(stderr, "Failed to parse check command\n");
    }

    if(age>100){
        fprintf(stderr, "Failed to parse check command: age is over the limit\n");
        return;
    }else if(age<0){
        fprintf(stderr, "Failed to parse insert command: age is over the limits\n");
        return ;
    }

    char* name = malloc(sizeof(char) * (strlen(command) + 1));
    strcpy(name, command);
    
    Node *copy = tree_find(tree, age, name);
    if(copy){
        printf("y\n");
    } else {
        printf("n\n");
    }
    free(name);

}
